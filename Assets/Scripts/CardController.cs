﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CardController :MonoBehaviour , IPointerClickHandler
{
	void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
	{
		Debug.Log("Clicked: " + eventData.pointerCurrentRaycast.gameObject.name);

		if (SceneManager.GetActiveScene().name == "PeelSessions") GameController.Instance.pickNextCard();
		else if (SceneManager.GetActiveScene().name == "Amiibos") AmiiboGameController.Instance.pickNextCard();
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : Singleton<GameController>
{

	private PeelSession[] sessions;

	[SerializeField]
	private int currentCardCount = 0;


    // Start is called before the first frame update
    void Start()
    {
		sessions = MyUtilities.LoadXMLData("peelsessions");
		DisplayCard(currentCardCount); ;		
	}

	public void pickNextCard()
	{
		currentCardCount++;
		if (currentCardCount == sessions.Length) currentCardCount = 0;

		DisplayCard(currentCardCount);
	}

	private void DisplayCard(int currentCardCount)
	{
		GameObject.Find("BandName").GetComponent<Text>().text = sessions[currentCardCount].BandName;
		GameObject.Find("BandImage").GetComponent<Image>().sprite = Resources.Load<Sprite>(sessions[currentCardCount].ImageName);

		GameObject.Find("NumberOfSessions").GetComponent<Text>().text = "Sessions Done: "+sessions[currentCardCount].NumberOfSessions;
		GameObject.Find("TotalSongs").GetComponent<Text>().text = "Songs Recorded: " +sessions[currentCardCount].NumberOfSongs;
		GameObject.Find("FirstSession").GetComponent<Text>().text = "First Session: " + sessions[currentCardCount].FirstSessionDate;
		GameObject.Find("LastSession").GetComponent<Text>().text = "Last Session:"+ sessions[currentCardCount].LastSessionDate;
	}
}

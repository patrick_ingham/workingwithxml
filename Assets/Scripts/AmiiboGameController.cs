﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmiiboGameController : Singleton<AmiiboGameController>
{

	private Amiibo[] amiibos;

	[SerializeField]
	private int currentCardCount = 0;


    // Start is called before the first frame update
    void Start()
    {
		amiibos = MyUtilities.LoadAmiiboXMLData("amiibos");
		DisplayCard(currentCardCount); ;		
	}

	public void pickNextCard()
	{
		currentCardCount++;
		if (currentCardCount == amiibos.Length) currentCardCount = 0;

		DisplayCard(currentCardCount);
	}

	private void DisplayCard(int currentCardCount)
	{
		GameObject.Find("AmiiboName").GetComponent<Text>().text = amiibos[currentCardCount].AmiiboName;
		GameObject.Find("AmiiboImage").GetComponent<Image>().sprite = Resources.Load<Sprite>(amiibos[currentCardCount].ImageName);

		GameObject.Find("SsbNumber").GetComponent<Text>().text = "Smash Number: "+amiibos[currentCardCount].SsbNumber;
		GameObject.Find("Versions").GetComponent<Text>().text = "Versions: " +amiibos[currentCardCount].Versions;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amiibo 
{
    private string amiiboName;
    private string imageName;
    private int ssbNumber;
    private int versions;
    

    public string AmiiboName { get => amiiboName; }
    public string ImageName { get => imageName; }
    public int SsbNumber { get => ssbNumber; }
    public int Versions { get => versions; }
   

    public Amiibo(string amiiboName, string imageName, int ssbNumber, int versions)
    {
        this.amiiboName = amiiboName;
        this.imageName = imageName;
        this.ssbNumber = ssbNumber;
        this.versions = versions;
       
    }
}
